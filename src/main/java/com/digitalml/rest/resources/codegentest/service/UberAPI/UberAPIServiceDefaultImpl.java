package com.digitalml.rest.resources.codegentest.service.UberAPI;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.security.Principal;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import javax.ws.rs.core.SecurityContext;
import com.google.common.base.Strings;

import com.digitalml.rest.resources.codegentest.service.UberAPIService;
	
/**
 * Default implementation for: Uber API
 * Move your app forward with the Uber API
 *
 * @author admin
 * @version 1.0.0
 */

public class UberAPIServiceDefaultImpl extends UberAPIService {


    public RetrieveHistoryByOffsetCurrentStateDTO retrieveHistoryByOffsetUseCaseStep1(RetrieveHistoryByOffsetCurrentStateDTO currentState) {
    

        RetrieveHistoryByOffsetReturnStatusDTO returnStatus = new RetrieveHistoryByOffsetReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Move your app forward with the Uber API");
	   	currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public RetrieveHistoryByOffsetCurrentStateDTO retrieveHistoryByOffsetUseCaseStep2(RetrieveHistoryByOffsetCurrentStateDTO currentState) {
    


        //No error condition, so just return the currentState
    	return currentState;
    };


    public RetrieveEstimatesPriceByStart_latitudeCurrentStateDTO retrieveEstimatesPriceByStartlatitudeUseCaseStep1(RetrieveEstimatesPriceByStart_latitudeCurrentStateDTO currentState) {
    

        RetrieveEstimatesPriceByStart_latitudeReturnStatusDTO returnStatus = new RetrieveEstimatesPriceByStart_latitudeReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Move your app forward with the Uber API");
	   	currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public RetrieveEstimatesPriceByStart_latitudeCurrentStateDTO retrieveEstimatesPriceByStartlatitudeUseCaseStep2(RetrieveEstimatesPriceByStart_latitudeCurrentStateDTO currentState) {
    


        //No error condition, so just return the currentState
    	return currentState;
    };


    public RetrieveMeCurrentStateDTO retrieveMeUseCaseStep1(RetrieveMeCurrentStateDTO currentState) {
    

        RetrieveMeReturnStatusDTO returnStatus = new RetrieveMeReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Move your app forward with the Uber API");
	   	currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public RetrieveMeCurrentStateDTO retrieveMeUseCaseStep2(RetrieveMeCurrentStateDTO currentState) {
    


        //No error condition, so just return the currentState
    	return currentState;
    };


    public RetrieveEstimatesTimeByStart_latitudeCurrentStateDTO retrieveEstimatesTimeByStartlatitudeUseCaseStep1(RetrieveEstimatesTimeByStart_latitudeCurrentStateDTO currentState) {
    

        RetrieveEstimatesTimeByStart_latitudeReturnStatusDTO returnStatus = new RetrieveEstimatesTimeByStart_latitudeReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Move your app forward with the Uber API");
	   	currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public RetrieveEstimatesTimeByStart_latitudeCurrentStateDTO retrieveEstimatesTimeByStartlatitudeUseCaseStep2(RetrieveEstimatesTimeByStart_latitudeCurrentStateDTO currentState) {
    


        //No error condition, so just return the currentState
    	return currentState;
    };


    public RetrieveProductsByLatitudeCurrentStateDTO retrieveProductsByLatitudeUseCaseStep1(RetrieveProductsByLatitudeCurrentStateDTO currentState) {
    

        RetrieveProductsByLatitudeReturnStatusDTO returnStatus = new RetrieveProductsByLatitudeReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Move your app forward with the Uber API");
	   	currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public RetrieveProductsByLatitudeCurrentStateDTO retrieveProductsByLatitudeUseCaseStep2(RetrieveProductsByLatitudeCurrentStateDTO currentState) {
    


        //No error condition, so just return the currentState
    	return currentState;
    };

	/**
	 * Creates and returns a {@link Method} object using reflection and handles the possible exceptions.
	 * <br/>
	 * Aids with calling the process step method based on the outcome of the control logic
	 * 
	 * @param methodName
	 * @param clazz
	 * @return
	 */
	private Method getMethod(String methodName, Class clazz) {
		Method method = null;
		try {
			method = UberAPIService.class.getMethod(methodName, clazz);
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		}

		return method;
	}
	
	/**
	 * For use when calling provider systems.
	 * <p>
	 * TODO: Implement security logic here
	 */
	protected SecurityContext securityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};
}