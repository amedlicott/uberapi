package com.digitalml.rest.resources.codegentest.service;

import java.security.Principal;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;


import com.digitalml.rest.resources.codegentest.service.UberAPI.UberAPIServiceDefaultImpl;
import com.digitalml.rest.resources.codegentest.service.UberAPIService.RetrieveProductsByLatitudeInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.UberAPIService.RetrieveProductsByLatitudeReturnDTO;
import javax.ws.rs.core.SecurityContext;

public class RetrieveProductsByLatitudeTests {

	@Test
	public void testOperationRetrieveProductsByLatitudeBasicMapping()  {
		UberAPIServiceDefaultImpl serviceDefaultImpl = new UberAPIServiceDefaultImpl();
		RetrieveProductsByLatitudeInputParametersDTO inputs = new RetrieveProductsByLatitudeInputParametersDTO();
		inputs.setLatitude(org.apache.commons.lang3.StringUtils.EMPTY);
		inputs.setLongitude(org.apache.commons.lang3.StringUtils.EMPTY);
		RetrieveProductsByLatitudeReturnDTO returnValue = serviceDefaultImpl.retrieveProductsByLatitude(fullyAutheticatedSecurityContext, inputs);
		
		assertNotNull(returnValue);				
	}
	

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};
}