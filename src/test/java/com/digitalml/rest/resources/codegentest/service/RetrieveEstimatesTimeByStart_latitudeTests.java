package com.digitalml.rest.resources.codegentest.service;

import java.security.Principal;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;


import com.digitalml.rest.resources.codegentest.service.UberAPI.UberAPIServiceDefaultImpl;
import com.digitalml.rest.resources.codegentest.service.UberAPIService.RetrieveEstimatesTimeByStart_latitudeInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.UberAPIService.RetrieveEstimatesTimeByStart_latitudeReturnDTO;
import javax.ws.rs.core.SecurityContext;

public class RetrieveEstimatesTimeByStart_latitudeTests {

	@Test
	public void testOperationRetrieveEstimatesTimeByStart_latitudeBasicMapping()  {
		UberAPIServiceDefaultImpl serviceDefaultImpl = new UberAPIServiceDefaultImpl();
		RetrieveEstimatesTimeByStart_latitudeInputParametersDTO inputs = new RetrieveEstimatesTimeByStart_latitudeInputParametersDTO();
		inputs.setStart_latitude(org.apache.commons.lang3.StringUtils.EMPTY);
		inputs.setStart_longitude(org.apache.commons.lang3.StringUtils.EMPTY);
		inputs.setCustomer_uuid(null);
		inputs.setProduct_id(null);
		RetrieveEstimatesTimeByStart_latitudeReturnDTO returnValue = serviceDefaultImpl.retrieveEstimatesTimeByStartlatitude(fullyAutheticatedSecurityContext, inputs);
		
		assertNotNull(returnValue);				
	}
	

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};
}