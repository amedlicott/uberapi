package com.digitalml.rest.resources.codegentest.service;

import java.security.Principal;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;


import com.digitalml.rest.resources.codegentest.service.UberAPI.UberAPIServiceDefaultImpl;
import com.digitalml.rest.resources.codegentest.service.UberAPIService.RetrieveMeInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.UberAPIService.RetrieveMeReturnDTO;
import javax.ws.rs.core.SecurityContext;

public class RetrieveMeTests {

	@Test
	public void testOperationRetrieveMeBasicMapping()  {
		UberAPIServiceDefaultImpl serviceDefaultImpl = new UberAPIServiceDefaultImpl();
		RetrieveMeInputParametersDTO inputs = new RetrieveMeInputParametersDTO();
		RetrieveMeReturnDTO returnValue = serviceDefaultImpl.retrieveMe(fullyAutheticatedSecurityContext, inputs);
		
		assertNotNull(returnValue);
		assertNotNull(returnValue.getResponseWrapper200());				
	}
	

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};
}